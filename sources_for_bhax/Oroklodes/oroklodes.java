class Parent{
	void message(){
		System.out.println("Hello there!");
	}
}

class Child extends Parent{
	@Override
	void message(){
		System.out.println("H3110 T1-13r3%");
	}
	void message2(){
		System.out.println("Hi there :)");
	}
}

class oroklodes{
	public static void main(String[] args) {
		Parent parent = new Parent();
		parent.message();
		parent=new Child();
		parent.message();
		Child child=new Child();
		child.message();
		child.message2();
		//Itt lathato, hogy a szulo csak a sajat uzenetet tudja elkuldeni
		//parent.message2();

	}
}

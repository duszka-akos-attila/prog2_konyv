#include <iostream>
#include <string>


class Parent
{
	public:
	void message(){
		std::cout << "Hello there!\n";
	}
};

class Child : public Parent
{
	public:
	void message2(){
		std::cout << "Hi there :)" << "\n";
	}
};


int main(int argc, char const *argv[])
{
	Parent* parent = new Parent();
	Parent* child= new Child();

	parent->message();
	child->message2();
	delete parent;
	delete child;
	return 0;
}

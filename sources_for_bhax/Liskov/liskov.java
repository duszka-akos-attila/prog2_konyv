interface Bird{
	 
}

interface flyingBird {
	abstract void fly();
}
class Owl  implements flyingBird{
	public void fly(){
		System.out.println("Your owl is crossing the sky!");
	}
}

class Chicken implements Bird{

}

class liskov{
	public static void main(String[] args) {
		Owl owl=new Owl();
		Chicken chicken=new Chicken();
		owl.fly();
		chicken.fly();
	}
}
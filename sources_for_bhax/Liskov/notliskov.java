class Bird{
	 void fly(){
		System.out.println("Your bird is an airplane in the sky!");
	}
}

class Owl extends Bird{
	@Override
	void fly(){
		System.out.println("Your owl is crossing the clouds!");
	}
}

class Chicken extends Bird{

}

class notliskov{
	public static void main(String[] args) {
		Bird bird=new Bird();
		bird.fly();
		bird=new Owl();
		bird.fly();
		bird=new Chicken();
		bird.fly();
	}
}
// ez a B program
class Bird {
public:
     virtual void fly() {};
};

// ez a két osztály alkotja a "C programot"
class Program {
public:
     void fgv ( Bird &bird ) {
          bird.fly();
     }
};

// most jon a ket C osztály
class Owl : public Bird
{};

class Chicken : public Bird // ezt úgy is lehet/kell olvasni, hogy a csirke tud repülni
{};

int main ( int argc, char **argv )
{
     Program program;
     Bird bird;
     program.fgv ( bird );

     Owl owl;
     program.fgv ( owl );

     Chicken chicken;
     program.fgv ( chicken ); // sérül az LSP, mert a B::fgv röptetné a csirket, ami ugye lehetetlen.

}

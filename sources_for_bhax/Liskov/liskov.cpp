
// ez a B
class Bird {
//public:
//  void fly(){};
};

// ez a két osztály alkotja a "C programot"
class Program {
public:
     void fgv ( Bird &bird ) {
          // bird.fly(); a madár már nem tud repülni
          // C hiába lesz a leszármazott típusoknak
          // repül metódusa, azt a Bird& bird-re nem lehet hívni
     }
};

// itt jönnek a C osztályok
class flyingBirds : public Bird {
public:
     virtual void fly() { };
};

class Owl : public flyingBirds
{
};

class Chicken : public Bird // ezt úgy is lehet/kell olvasni, hogy a pingvin tud repülni
{};

int main ( int argc, char **argv )
{
     Program program;
     Bird bird;
     program.fgv ( bird );

     Owl owl;
     program.fgv ( owl );
     owl.repul();
     Chicken chicken;
     program.fgv ( chicken );
     chicken.repul();

}
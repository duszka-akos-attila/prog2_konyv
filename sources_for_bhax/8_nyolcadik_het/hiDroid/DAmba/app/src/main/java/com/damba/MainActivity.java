package com.damba;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.damba.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button[][] buttons=new Button[5][5];

    private boolean p1Turn = true;

    private int roundNum;

    private int p1Points;
    private int p2Points;

    private TextView textViewPlayer1;
    private TextView textViewPlayer2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewPlayer1 = findViewById(R.id.text_view_p1);
        textViewPlayer2 = findViewById(R.id.text_view_p2);

        for(int i =0; i<5; i++){
            for (int j=0; j<5; j++){
                String buttonID= "button_"+i+j;
                int resID= getResources().getIdentifier(buttonID, "id", getPackageName());
                buttons[i][j]= findViewById(resID);
                buttons[i][j].setOnClickListener(this);

            }
        }

        Button buttonReset = findViewById(R.id.button_reset);
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetGame();
            }
        });

    }

    @Override
    public void onClick(View v) {

        if(!((Button) v).getText().toString().equals(""))
        {
            return;
        }

        if(p1Turn){
            ((Button) v).setText("X");
        }

        else{
            ((Button) v).setText("O");
        }

        roundNum++;

        if(checkWin()){
            if(p1Turn){
                p1Win();
            }
            else{
                p2Win();
            }
        }

        else if(roundNum==25){
            draw();
        }

        else{
            p1Turn= !p1Turn;
        }
    }

    private boolean checkWin(){

        String[][] field = new String[5][5];

        for(int i=0; i<5; i++){
            for(int j=0; j<5; j++){

                field[i][j] = buttons[i][j].getText().toString();

            }
        }

        for(int i=0; i<5; i++){
            if(field[i][0].equals(field[i][1]) && field[i][0].equals(field[i][2]) && field[i][0].equals(field[i][3]) && field[i][0].equals(field[i][4]) && !field[i][0].equals("")){
                return true;
            }
        }

        for(int i=0; i<5; i++){
            if(field[0][i].equals(field[1][i]) && field[0][i].equals(field[2][i]) && field[0][i].equals(field[3][i]) && field[0][i].equals(field[4][i]) && !field[0][i].equals("")){
                return true;
            }
        }

        if(field[0][0].equals(field[1][1]) && field[0][0].equals(field[2][2]) && field[0][0].equals(field[3][3]) && field[0][0].equals(field[4][4]) && !field[0][0].equals("")){
            return true;
        }

        if(field[0][4].equals(field[1][3]) && field[0][4].equals(field[2][2]) && field[0][4].equals(field[3][1]) && field[0][4].equals(field[4][0]) && !field[0][4].equals("")){
            return true;
        }

        return false;
    }

    private void p1Win(){
        p1Points++;
        Toast.makeText(this, "Játékos 1 nyert!", Toast.LENGTH_SHORT).show();
        updatePointsText();
        resetBoard();
    }

    private void p2Win(){
        p2Points++;
        Toast.makeText(this, "Játékos 2 nyert!", Toast.LENGTH_SHORT).show();
        updatePointsText();
        resetBoard();
    }

    private void draw(){
        Toast.makeText(this, "Döntetlen!", Toast.LENGTH_SHORT).show();
        resetBoard();
    }

    private void updatePointsText(){
        textViewPlayer1.setText("Játékos 1: "+p1Points+"p");
        textViewPlayer2.setText("Játékos 2: "+p2Points+"p");
    }

    private void resetBoard(){
        for (int i=0; i<5; i++){
            for (int j=0; j<5; j++){
                buttons[i][j].setText("");
            }
        }

        roundNum= 0;
        p1Turn=true;

    }

    private void resetGame(){
        p1Points=0;
        p2Points=0;
        updatePointsText();
        resetBoard();;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("roundNum",roundNum);
        outState.putInt("p1Points",p1Points);
        outState.putInt("p2Points",p2Points);
        outState.putBoolean("p1Turn",p1Turn);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        roundNum= savedInstanceState.getInt("roundNum");
        p1Points= savedInstanceState.getInt("p1Points");
        p2Points= savedInstanceState.getInt("p2Points");
        p1Turn= savedInstanceState.getBoolean("p1Turn");
    }
}


#ifndef LZWBINFA_H
#define LZWBINFA_H

#include string

/**
  * class LZWBinFa
  * Az LZWBinFa osztályban absztraháljuk az LZW algoritmus bináris fa építését. Az
  * osztály
  * definíciójába beágyazzuk a fa egy csomópontjának az absztrakt jellemzését, ez
  * lesz a
  * beágyazott Csomopont osztály. Miért ágyazzuk be? Mert külön nem szánunk neki
  * szerepet, ezzel
  * is jelezzük, hogy csak a fa részeként számiolunk vele.
  */

class LZWBinFa
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  LZWBinFa ();

  /**
   * Empty Destructor
   */
  virtual ~LZWBinFa ();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * @param  b
   */
  void operator_ (char b)
  {
  }


  /**
   */
  void kiir ()
  {
  }


  /**
   * A változatosság kedvéért ezeket az osztálydefiníció (class LZWBinFa {...};) után
   * definiáljuk,
   * hogy kénytelen légy az LZWBinFa és a :: hatókör operátorral minősítve definiálni
   * :) l. lentebb
   * @return int
   */
  int getMelyseg ()
  {
  }


  /**
   * @return double
   */
  double getAtlag ()
  {
  }


  /**
   * @return double
   */
  double getSzoras ()
  {
  }


  /**
   * @return std::ostream&
   * @param  os
   * @param  bf
   */
  std::ostream& operator_ (std::ostream& os, LZWBinFa& bf)
  {
  }


  /**
   * @param  os
   */
  void kiir (std::ostream& os)
  {
  }

protected:

  // Static Protected attributes
  //  

  // Protected attributes
  //  

  // A fában tagként benne van egy csomópont, ez erősen ki van tüntetve, Ő a gyökér: 
  Csomopont gyoker;
  int maxMelyseg;
  double atlag;
  double szoras;
public:


  // Protected attribute accessor methods
  //  

protected:

public:


  // Protected attribute accessor methods
  //  


  /**
   * Set the value of gyoker
   * A fában tagként benne van egy csomópont, ez erősen ki van tüntetve, Ő a gyökér:
   * @param new_var the new value of gyoker
   */
  void setGyoker (Csomopont new_var)   {
      gyoker = new_var;
  }

  /**
   * Get the value of gyoker
   * A fában tagként benne van egy csomópont, ez erősen ki van tüntetve, Ő a gyökér:
   * @return the value of gyoker
   */
  Csomopont getGyoker ()   {
    return gyoker;
  }

  /**
   * Set the value of maxMelyseg
   * @param new_var the new value of maxMelyseg
   */
  void setMaxMelyseg (int new_var)   {
      maxMelyseg = new_var;
  }

  /**
   * Get the value of maxMelyseg
   * @return the value of maxMelyseg
   */
  int getMaxMelyseg ()   {
    return maxMelyseg;
  }

  /**
   * Set the value of atlag
   * @param new_var the new value of atlag
   */
  void setAtlag (double new_var)   {
      atlag = new_var;
  }

  /**
   * Get the value of atlag
   * @return the value of atlag
   */
  double getAtlag ()   {
    return atlag;
  }

  /**
   * Set the value of szoras
   * @param new_var the new value of szoras
   */
  void setSzoras (double new_var)   {
      szoras = new_var;
  }

  /**
   * Get the value of szoras
   * @return the value of szoras
   */
  double getSzoras ()   {
    return szoras;
  }
protected:



  /**
   * @param  elem
   */
  void rmelyseg (Csomopont* elem)
  {
  }


  /**
   * @param  elem
   */
  void ratlag (Csomopont* elem)
  {
  }


  /**
   * @param  elem
   */
  void rszoras (Csomopont* elem)
  {
  }

private:

  // Static Private attributes
  //  

  // Private attributes
  //  

  // Mindig a fa "LZW algoritmus logikája szerinti aktuális" csomópontjára mutat 
  Csomopont* fa;
  // technikai
  int melyseg;
  int atlagosszeg;
  int atlagdb;
  double szorasosszeg;
public:


  // Private attribute accessor methods
  //  

private:

public:



#ifndef STD_H
#define STD_H

#include string

/**
  * class std
  * 
  */

class std
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  std ();

  /**
   * Empty Destructor
   */
  virtual ~std ();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  


protected:

  // Static Protected attributes
  //  

  // Protected attributes
  //  

public:


  // Protected attribute accessor methods
  //  

protected:

public:


  // Protected attribute accessor methods
  //  

protected:


private:

  // Static Private attributes
  //  

  // Private attributes
  //  

public:


  // Private attribute accessor methods
  //  

private:

public:


  // Private attribute accessor methods
  //  

private:



};

#endif // STD_H

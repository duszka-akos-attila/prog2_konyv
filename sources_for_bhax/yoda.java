public class yoda{
	String name;
	String secondName;
	
	public yoda(){
		this.name="Yoda";
		this.secondName=null;
	}	
	
	public static boolean normalCompare(yoda yoda1){
		boolean returnValue=false;
		System.out.println("--------------------------------------------------");
		System.out.println("!START -COMPARE- normal");
		try{
			if(yoda1.secondName.equals("Jedi")){
				returnValue=true;
			}
			
			System.out.println("!STOP -COMPARE- normal");
			System.out.println("RESULT: "+returnValue);
			System.out.println("--------------------------------------------------");
		}
		
		catch(Exception e){
			System.out.println("");
			System.out.println("##################################################");
			System.out.println("!ERROR IN: -COMPARE- RESULT:"+returnValue);
			System.out.println("[ERROR DETAILS] > "+e);
			System.out.println("##################################################");
			System.out.println("");
			System.out.println("--------------------------------------------------");
		}
		
		System.out.println("");
		
		return returnValue;
		
	}
	
	public static boolean myCompare(yoda yoda2){
		boolean returnValue=false;
		System.out.println("--------------------------------------------------");
		System.out.println("!START -COMPARE- yoda");
		try{
			if("Master".equals(yoda2.secondName)){
				returnValue=true;
			}
			
			System.out.println("!STOP -COMPARE- yoda");
			System.out.println("RESULT: "+returnValue);
			System.out.println("--------------------------------------------------");
		}
		
		catch(Exception e){
			System.out.println("##################################################");
			System.out.println("!ERROR IN: -COMPARE- RESULT:"+returnValue);
			System.out.println("[ERROR DETAILS] > "+e);
			System.out.println("##################################################");
			System.out.println("");
			System.out.println("--------------------------------------------------");
		}
		
		System.out.println("");
		
		return returnValue;
		
	}
	
	public static void main(String[] args){
	
		yoda yoda3=new yoda();
		normalCompare(yoda3);
		myCompare(yoda3);
	
	}
}

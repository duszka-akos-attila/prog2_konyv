import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;


public class Fullscreen {

    public static void main(String[] args) {
        leetgen();
    }
    
//---------------------------------------------------------------------------------
    
    
//----------------------------------------------------------------------    


    
//------------------------------------------------------------------------------------------------------------------    
    
    //Egyjátékos mód
    public static void leetgen(){

	GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();

	KeyListener listener = new KeyListener() {
		
		@Override
		public void keyPressed(KeyEvent event){

			if(event.getKeyCode() == KeyEvent.VK_ESCAPE)		
				System.exit(0);
		}

		@Override
		public void keyReleased(KeyEvent event){}

		@Override
		public void keyTyped(KeyEvent event){}
	};


        //Új frame létrehozása az egyjátékos módhoz
        JFrame options = new JFrame("");
        options.setTitle("Leet generator");
	    options.addKeyListener(listener);
       // options.setSize(600, 200);
        options.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Textfieldek bekéréshez, gomb a kezdéshez és a felület kirajzolása
        JTextField atalakitando = new JTextField("Adja meg, az átalakítandó szöveget");
        JTextField generalt = new JTextField("Itt fog megjelenni a generált szöveg...");
        generalt.setEditable(false);
        JButton start = new JButton("Generál");
        options.getContentPane().add(BorderLayout.NORTH,atalakitando);
        options.getContentPane().add(BorderLayout.CENTER,generalt);
        options.getContentPane().add(BorderLayout.SOUTH,start);
        atalakitando.addKeyListener(listener);
        generalt.addKeyListener(listener);


	        atalakitando.setSize(400, 200);
	generalt.setSize(600, 600);
        
	if (gd.isFullScreenSupported()) {
		options.setUndecorated(true);
		gd.setFullScreenWindow(options);
	}
	else{
		System.err.println("Hiba a teljesképernyős móddal");
		options.setSize(600, 200);
		options.setVisible(true);
	}
	
	

        //ha megkaptuk a számot akkor elkezdődik a játék       
        start.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(atalakitando.getText().equals(null)){
                    
                    generalt.setText("Nem írtál be semmit!");
                        
                }
                else{
                    //létrehozzuk a szükséges dolgokat
                String atalakitandos= atalakitando.getText();
                
                //Előző gombok eltávolitása
                /*start.setVisible(false);
                atalakitando.setVisible(false);*/
             //   options.setSize(800,800);

                legen legen =new legen(atalakitandos);
                generalt.setText(legen.legeneral());


                KeyListener listener = new KeyListener() {
        
        @Override
        public void keyPressed(KeyEvent event){

            if(event.getKeyCode() == KeyEvent.VK_ESCAPE)            
                System.exit(0);
        }

        @Override
        public void keyReleased(KeyEvent event){}

        @Override
        public void keyTyped(KeyEvent event){}
    };
           
                
                }                  
            }
        });  
    }
    
}
